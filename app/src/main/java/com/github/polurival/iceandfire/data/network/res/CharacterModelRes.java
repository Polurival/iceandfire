package com.github.polurival.iceandfire.data.network.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public class CharacterModelRes {

    @SerializedName("url")
    @Expose
    private String url;
    public String getUrl() {
        return url;
    }

    @SerializedName("name")
    @Expose
    private String name;
    public String getName() {
        return name;
    }

    @SerializedName("born")
    @Expose
    private String born;
    public String getBorn() {
        return born;
    }

    @SerializedName("died")
    @Expose
    private String died;
    public String getDied() {
        return died;
    }

    @SerializedName("titles")
    @Expose
    private List<String> titles = new ArrayList<String>();
    public List<String> getTitles() {
        return titles;
    }

    @SerializedName("aliases")
    @Expose
    private List<String> aliases = new ArrayList<String>();
    public List<String> getAliases() {
        return aliases;
    }

    @SerializedName("father")
    @Expose
    private String father;
    public String getFather() {
        return father;
    }

    @SerializedName("mother")
    @Expose
    private String mother;
    public String getMother() {
        return mother;
    }

    @SerializedName("allegiances")
    @Expose
    private List<String> allegiances = new ArrayList<String>();
    public List<String> getAllegiances() {
        return allegiances;
    }
}
