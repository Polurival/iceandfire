package com.github.polurival.iceandfire.data.storage.models;

/**
 * Created by Polurival
 * on 16.10.2016.
 */

public class CharacterDetailModel {

    private String mName;
    private String mWords;
    private String mBorn;
    private String mDied;
    private String mTitles;
    private String mAliases;
    private String mFather;
    private String mMother;

    public CharacterDetailModel(String name, String words, String born, String died,
                                String titles, String aliases, String father, String mother) {
        mName = name;
        mWords = words;
        mBorn = born;
        mDied = died;
        mTitles = titles;
        mAliases = aliases;
        mFather = father;
        mMother = mother;
    }

    public String getName() {
        return mName;
    }

    public String getWords() {
        return mWords;
    }

    public String getBorn() {
        return mBorn;
    }

    public String getDied() {
        return mDied;
    }

    public String getTitles() {
        return mTitles;
    }

    public String getAliases() {
        return mAliases;
    }

    public String getFather() {
        return mFather;
    }

    public String getMother() {
        return mMother;
    }
}
