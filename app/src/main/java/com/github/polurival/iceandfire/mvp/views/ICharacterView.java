package com.github.polurival.iceandfire.mvp.views;

/**
 * Created by Polurival
 * on 24.10.2016.
 */

public interface ICharacterView {

    void setTitle(String name);

    void setLannisterLogo();

    void setStarkLogo();

    void setTargaryenLogo();

    void setWords(String words);

    void setBorn(String born);

    void setDied(String died);

    void setTitles(String titles);

    void setAliases(String aliases);

    void setFather(String father);

    void setMother(String mother);
}
