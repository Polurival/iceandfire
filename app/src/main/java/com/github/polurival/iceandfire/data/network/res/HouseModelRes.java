package com.github.polurival.iceandfire.data.network.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public class HouseModelRes {

    @SerializedName("url")
    @Expose
    public String url;
    public String getUrl() {
        return url;
    }

    @SerializedName("words")
    @Expose
    public String words;
    public String getWords() {
        return words;
    }

    @SerializedName("swornMembers")
    @Expose
    public List<String> swornMembers = new ArrayList<String>();
    public List<String> getSwornMembers() {
        return swornMembers;
    }
}
