package com.github.polurival.iceandfire.ui.activities;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.polurival.iceandfire.R;
import com.github.polurival.iceandfire.mvp.presenters.CharacterPresenter;
import com.github.polurival.iceandfire.mvp.views.ICharacterView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.github.polurival.iceandfire.utils.ConstantManager.CHARACTER_URL;
import static com.github.polurival.iceandfire.utils.ConstantManager.HOUSE_ID;
import static com.github.polurival.iceandfire.utils.ConstantManager.LOG_PREFIX;

public class CharacterActivity extends AppCompatActivity implements ICharacterView {

    private static final String TAG = LOG_PREFIX + CharacterActivity.class.getSimpleName();

    private CharacterPresenter mCharacterPresenter = CharacterPresenter.getInstance();

    @BindView(R.id.collapsing_toolbar_character)
    CollapsingToolbarLayout mCollapsingToolbar;

    @BindView(R.id.logo_character)
    ImageView mLogo;

    @BindView(R.id.words_character)
    TextView mTvWords;

    @BindView(R.id.born_character)
    TextView mTvBorn;

    @BindView(R.id.died_character)
    TextView mTvDied;

    @BindView(R.id.titles_character)
    TextView mTvTitles;

    @BindView(R.id.aliases_character)
    TextView mTvAliases;

    @BindView(R.id.father_character)
    TextView mTvFather;

    @BindView(R.id.mother_character)
    TextView mTvMother;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
        ButterKnife.bind(this);

        mCharacterPresenter.takeView(this);

        String url = getIntent().getStringExtra(CHARACTER_URL);
        String houseId = getIntent().getStringExtra(HOUSE_ID);
        Log.d(TAG, "mCharacter url: " + url + " houseId: " + houseId);

        setupToolbar();

        mCharacterPresenter.setHouseId(houseId);
        mCharacterPresenter.getCharacterFromDB(url);

        mCharacterPresenter.setTitle();
        mCharacterPresenter.initLogo();
        mCharacterPresenter.initTextViews();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mCharacterPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public void setTitle(String name) {
        mCollapsingToolbar.setTitle(name);
    }

    @Override
    public void setLannisterLogo() {
        mLogo.setImageResource(R.drawable.lannister);
    }

    @Override
    public void setStarkLogo() {
        mLogo.setImageResource(R.drawable.stark);
    }

    @Override
    public void setTargaryenLogo() {
        mLogo.setImageResource(R.drawable.targaryen);
    }

    @Override
    public void setWords(String words) {
        mTvWords.setText(words);
    }

    @Override
    public void setBorn(String born) {
        mTvBorn.setText(born);
    }

    @Override
    public void setDied(String died) {
        mTvDied.setText(died);
    }

    @Override
    public void setTitles(String titles) {
        mTvTitles.setText(titles);
    }

    @Override
    public void setAliases(String aliases) {
        mTvAliases.setText(aliases);
    }

    @Override
    public void setFather(String father) {
        mTvFather.setText(father);
    }

    @Override
    public void setMother(String mother) {
        mTvMother.setText(mother);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_character);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
