package com.github.polurival.iceandfire.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;

import com.github.polurival.iceandfire.R;
import com.github.polurival.iceandfire.mvp.presenters.SplashPresenter;
import com.github.polurival.iceandfire.mvp.views.ISplashView;

import static com.github.polurival.iceandfire.utils.AppConfig.SPLASH_ACTIVITY_DELAY;
import static com.github.polurival.iceandfire.utils.ConstantManager.LOG_PREFIX;

public class SplashActivity extends AppCompatActivity implements ISplashView {

    private static final String TAG = LOG_PREFIX + SplashActivity.class.getSimpleName();

    private SplashPresenter mSplashPresenter = SplashPresenter.getInstance();
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        mSplashPresenter.takeView(this);

        showProgress();

        mSplashPresenter.loadHouses();
        mSplashPresenter.startMainActivityWithDelay();
    }

    @Override
    protected void onDestroy() {
        mSplashPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            Window window = mProgressDialog.getWindow();
            if (window != null) {
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_splash);
        } else {
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_splash);
        }
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void startMainActivityWithDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSplashPresenter.startMainActivity();
            }
        }, SPLASH_ACTIVITY_DELAY);
    }

    @Override
    public void startMainActivity() {
        hideProgress();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
