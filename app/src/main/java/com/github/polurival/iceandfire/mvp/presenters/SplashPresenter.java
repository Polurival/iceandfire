package com.github.polurival.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;

import com.github.polurival.iceandfire.data.managers.DataManager;
import com.github.polurival.iceandfire.mvp.models.SplashModel;
import com.github.polurival.iceandfire.mvp.views.ISplashView;

import static com.github.polurival.iceandfire.utils.ConstantManager.HOUSES;

/**
 * Created by Polurival
 * on 23.10.2016.
 */

public class SplashPresenter implements ISplashPresenter, DataManager.OnDataPreparedListener {

    private static SplashPresenter sSplashPresenter = new SplashPresenter();
    private SplashModel mSplashModel;
    private ISplashView mSplashView;

    private SplashPresenter() {
        mSplashModel = new SplashModel();
        mSplashModel.setOnDataPreparedListener(this);
    }

    public static SplashPresenter getInstance() {
        return sSplashPresenter;
    }

    @Override
    public void takeView(ISplashView splashView) {
        mSplashView = splashView;
    }

    @Override
    public void dropView() {
        mSplashView = null;
    }

    @Nullable
    @Override
    public ISplashView getView() {
        return mSplashView;
    }

    @Override
    public void startMainActivityWithDelay() {
        if (getView() != null) {
            if (isDBPrepared()) {
                getView().startMainActivityWithDelay();
            }
        }
    }

    @Override
    public void startMainActivity() {
        if (getView() != null) {
            getView().startMainActivity();
        }
    }

    @Override
    public boolean isDBPrepared() {
        return mSplashModel.isDBPrepared();
    }

    @Override
    public void loadHouses() {
        if (!isDBPrepared()) {
            mSplashModel.loadHouse(0);
        }
    }

    @Override
    public void checkLastRequestAndStartMainActivity(int houseDBId, int characterDBid,
                                                     int membersCount) {
        if (houseDBId == HOUSES.length - 1 && characterDBid == membersCount - 1) {
            mSplashModel.saveDBPreparedFlag();
            startMainActivity();
        } else if (characterDBid == membersCount - 1) {
            mSplashModel.loadHouse(houseDBId + 1);
        }
    }
}
