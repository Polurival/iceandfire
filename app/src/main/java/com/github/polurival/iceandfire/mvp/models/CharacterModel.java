package com.github.polurival.iceandfire.mvp.models;

import com.github.polurival.iceandfire.data.managers.DataManager;
import com.github.polurival.iceandfire.data.storage.models.CharacterDetailModel;

/**
 * Created by Polurival
 * on 24.10.2016.
 */

public class CharacterModel {

    public CharacterModel() {
    }

    public CharacterDetailModel getCharacterFromDB(String url, String houseId) {
        return DataManager.getInstance().getCharacterFromDB(url, houseId);
    }
}
