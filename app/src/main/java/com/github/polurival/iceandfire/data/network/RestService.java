package com.github.polurival.iceandfire.data.network;

import com.github.polurival.iceandfire.data.network.res.CharacterModelRes;
import com.github.polurival.iceandfire.data.network.res.HouseModelRes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public interface RestService {

    @GET("houses/{houseId}")
    Call<HouseModelRes> getHouse(@Path("houseId") String houseId);

    @GET("characters/{characterId}")
    Call<CharacterModelRes> getCharacter(@Path("characterId") String characterId);
}
