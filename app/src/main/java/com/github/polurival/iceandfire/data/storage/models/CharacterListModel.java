package com.github.polurival.iceandfire.data.storage.models;

/**
 * Created by Polurival
 * on 16.10.2016.
 */

public class CharacterListModel {

    private String mUrl;
    private String mName;
    private String mTitles;

    public CharacterListModel(String url, String name, String titles) {
        mUrl = url;
        mName = name;
        mTitles = titles;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getName() {
        return mName;
    }

    public String getTitles() {
        return mTitles;
    }
}
