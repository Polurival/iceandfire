package com.github.polurival.iceandfire.utils;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public interface AppConfig {
    String BASE_URL = "http://www.anapioficeandfire.com/api/";
    int SPLASH_ACTIVITY_DELAY = 3000;
}
