package com.github.polurival.iceandfire.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.github.polurival.iceandfire.R;
import com.github.polurival.iceandfire.mvp.presenters.MainPresenter;
import com.github.polurival.iceandfire.mvp.views.IMainView;
import com.github.polurival.iceandfire.ui.adapters.TabsFragmentAdapter;

import static com.github.polurival.iceandfire.utils.ConstantManager.LOG_PREFIX;
import static com.github.polurival.iceandfire.utils.ConstantManager.TAB_LANNISTER;
import static com.github.polurival.iceandfire.utils.ConstantManager.TAB_TARGARYEN;
import static com.github.polurival.iceandfire.utils.ConstantManager.TAB_STARK;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public class MainActivity extends AppCompatActivity implements IMainView {

    private static final String TAG = LOG_PREFIX + MainActivity.class.getSimpleName();

    private MainPresenter mMainPresenter = MainPresenter.getInstance();

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainPresenter.takeView(this);

        initDrawerLayout();
        initToolbar();
        initViewPager();
        initTabLayout();
        initNavigationView();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)) {
            mDrawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mMainPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setAdapterToViewPager(TabsFragmentAdapter adapter) {
        mViewPager.setAdapter(adapter);
    }

    @Override
    public void showLannisterTab() {
        mViewPager.setCurrentItem(TAB_LANNISTER);
    }

    @Override
    public void showStarkTab() {
        mViewPager.setCurrentItem(TAB_STARK);
    }

    @Override
    public void showTargaryenTab() {
        mViewPager.setCurrentItem(TAB_TARGARYEN);
    }

    private void initDrawerLayout() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_toggle_open, R.string.drawer_toggle_close);
        mDrawerLayout.setDrawerListener(mToggle);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initViewPager() {
        Log.d(TAG, "initViewPager");

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mMainPresenter.setAdapterToViewPager();
    }

    private void initTabLayout() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private void initNavigationView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem item) {
                        item.setChecked(true);
                        mDrawerLayout.closeDrawers();

                        switch (item.getItemId()) {
                            case R.id.action_lannister_item:
                                mMainPresenter.clickOnLannisterTab();
                                break;
                            case R.id.action_stark_item:
                                mMainPresenter.clickOnStarkTab();
                                break;
                            case R.id.action_targaryen_item:
                                mMainPresenter.clickOnTargaryenTab();
                        }
                        return true;
                    }
                });
    }
}
