package com.github.polurival.iceandfire.data.managers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import com.github.polurival.iceandfire.data.network.RestService;
import com.github.polurival.iceandfire.data.network.ServiceGenerator;
import com.github.polurival.iceandfire.data.network.res.CharacterModelRes;
import com.github.polurival.iceandfire.data.network.res.HouseModelRes;
import com.github.polurival.iceandfire.data.storage.DBHelper;
import com.github.polurival.iceandfire.data.storage.models.CharacterDetailModel;
import com.github.polurival.iceandfire.data.storage.models.CharacterListModel;
import com.github.polurival.iceandfire.utils.IceAndFireApp;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.github.polurival.iceandfire.utils.ConstantManager.Column;
import static com.github.polurival.iceandfire.utils.ConstantManager.HOUSES;
import static com.github.polurival.iceandfire.utils.ConstantManager.Table;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public class DataManager {

    private static final String TAG = DataManager.class.getSimpleName();

    private static DataManager INSTANCE = null;

    private RestService mRestService;
    private DBHelper mDBHelper;

    private DataManager() {
        mRestService = ServiceGenerator.createService(RestService.class);
        mDBHelper = DBHelper.getInstance(IceAndFireApp.getContext());
    }

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public interface OnDataPreparedListener {
        void checkLastRequestAndStartMainActivity(int houseDBId, int characterDBid,
                                                  int membersCount);
    }

    private OnDataPreparedListener mOnDataPreparedListener;

    public void setOnDataPreparedListener(OnDataPreparedListener listener) {
        mOnDataPreparedListener = listener;
    }

    //todo изменить логику скачивания и сохранения, сейчас иногда наблюдается баг
    public void loadHouse(int housePos) {
        //for (int i = 0; i < HOUSES.length; i++) {
            Call<HouseModelRes> houseCall = getHouse(HOUSES[housePos]);

            final int houseDBId = housePos;
            houseCall.enqueue(new Callback<HouseModelRes>() {
                @Override
                public void onResponse(Call<HouseModelRes> call,
                                       Response<HouseModelRes> response) {
                    HouseModelRes house = response.body();
                    insertHouse(house);

                    loadHouseCharacters(house.getSwornMembers(), houseDBId);
                }

                @Override
                public void onFailure(Call<HouseModelRes> call, Throwable t) {
                }
            });
        //}
    }

    private void loadHouseCharacters(List<String> swornMembers, final int houseDBId) {
        final int membersCount = swornMembers.size();

        Log.i(TAG, "Members in house: " + membersCount);
        for (int i = 0; i < membersCount; i++) {
            String[] urlParts = swornMembers.get(i).split("/");
            String characterId = urlParts[urlParts.length - 1];

            final int characterDBid = i;
            Call<CharacterModelRes> characterCall = getCharacter(characterId);
            characterCall.enqueue(new Callback<CharacterModelRes>() {
                @Override
                public void onResponse(Call<CharacterModelRes> call,
                                       Response<CharacterModelRes> response) {
                    CharacterModelRes character = response.body();
                    insertCharacter(character);

                    mOnDataPreparedListener.checkLastRequestAndStartMainActivity(houseDBId,
                            characterDBid, membersCount);
                }

                @Override
                public void onFailure(Call<CharacterModelRes> call, Throwable t) {
                }
            });
        }
    }

    private Call<HouseModelRes> getHouse(String houseId) {
        return mRestService.getHouse(houseId);
    }

    private Call<CharacterModelRes> getCharacter(String characterId) {
        return mRestService.getCharacter(characterId);
    }

    private void insertHouse(HouseModelRes house) {
        ContentValues cv = new ContentValues();

        cv.put(Column.URL, house.getUrl());
        cv.put(Column.WORDS, house.getWords());

        mDBHelper.getWritableDatabase().insert(Table.HOUSE, null, cv);
        Log.d(TAG, "House " + house.getUrl() + " was inserted in DB");
    }

    private void insertCharacter(CharacterModelRes character) {
        ContentValues cv = new ContentValues();

        cv.put(Column.URL, character.getUrl());
        cv.put(Column.NAME, character.getName());
        cv.put(Column.BORN, character.getBorn());
        cv.put(Column.DIED, character.getDied());

        putTitlesToCv(character, cv);

        putAliasesToCv(character, cv);

        cv.put(Column.FATHER, character.getFather());
        cv.put(Column.MOTHER, character.getMother());

        putAllegiancesToCv(character, cv);

        mDBHelper.getWritableDatabase().insert(Table.CHARACTER, null, cv);
        Log.d(TAG, "Character " + character.getUrl() + " was inserted in DB");
    }

    private void putTitlesToCv(CharacterModelRes character, ContentValues cv) {
        List<String> titles = character.getTitles();
        if (titles != null && titles.size() > 0) {
            String titlesStr = TextUtils.join("\n", titles);
            cv.put(Column.TITLES, titlesStr);
        } else {
            cv.put(Column.TITLES, "");
        }
    }

    private void putAliasesToCv(CharacterModelRes character, ContentValues cv) {
        List<String> aliases = character.getAliases();
        if (aliases != null && aliases.size() > 0) {
            String aliasesStr = TextUtils.join("\n", aliases);
            cv.put(Column.AlIASES, aliasesStr);
        } else {
            cv.put(Column.AlIASES, "");
        }
    }

    private void putAllegiancesToCv(CharacterModelRes character, ContentValues cv) {
        List<String> allegiances = character.getAllegiances();
        if (allegiances != null && allegiances.size() > 0) {
            String allegiancesStr = TextUtils.join("\n", allegiances);
            cv.put(Column.ALLEGIANCES, allegiancesStr);
        }
    }

    public List<CharacterListModel> getCharactersFromDB(String houseId) {
        Cursor cursor = mDBHelper.getReadableDatabase().query(
                true,
                Table.CHARACTER,
                new String[]{Column.URL, Column.NAME, Column.TITLES},
                Column.ALLEGIANCES + " LIKE '%" + houseId + "%'",
                null,
                null,
                null,
                Column.NAME,
                null
        );

        List<CharacterListModel> charactersList = new ArrayList<>();
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            String url = cursor.getString(cursor.getColumnIndex(Column.URL));
            String name = cursor.getString(cursor.getColumnIndex(Column.NAME));
            String titles = cursor.getString(cursor.getColumnIndex(Column.TITLES));

            charactersList.add(new CharacterListModel(url, name, titles));
        }
        cursor.close();

        return charactersList;
    }

    public CharacterDetailModel getCharacterFromDB(String characterUrl, String houseId) {
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        Cursor houseCursor = db.query(
                Table.HOUSE,
                new String[]{Column.WORDS},
                Column.URL + " LIKE '%" + houseId + "%'",
                null,
                null,
                null,
                null
        );
        Cursor characterCursor = db.query(
                Table.CHARACTER,
                new String[]{Column.NAME, Column.BORN, Column.DIED, Column.TITLES,
                        Column.AlIASES, Column.FATHER, Column.MOTHER},
                Column.URL + " = ?",
                new String[]{characterUrl},
                null,
                null,
                null
        );

        houseCursor.moveToFirst();
        String words = houseCursor.getString(houseCursor.getColumnIndex(Column.WORDS));

        characterCursor.moveToFirst();
        String name = characterCursor.getString(characterCursor.getColumnIndex(Column.NAME));
        String born = characterCursor.getString(characterCursor.getColumnIndex(Column.BORN));
        String died = characterCursor.getString(characterCursor.getColumnIndex(Column.DIED));
        String titles = characterCursor.getString(characterCursor.getColumnIndex(Column.TITLES));
        String aliases = characterCursor.getString(characterCursor.getColumnIndex(Column.AlIASES));
        String father = characterCursor.getString(characterCursor.getColumnIndex(Column.FATHER));
        String mother = characterCursor.getString(characterCursor.getColumnIndex(Column.MOTHER));

        CharacterDetailModel character =
                new CharacterDetailModel(name, words, born, died, titles, aliases, father, mother);

        houseCursor.close();
        characterCursor.close();

        return character;
    }
}
