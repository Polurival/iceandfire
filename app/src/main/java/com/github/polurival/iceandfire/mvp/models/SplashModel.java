package com.github.polurival.iceandfire.mvp.models;

import com.github.polurival.iceandfire.data.managers.DataManager;
import com.github.polurival.iceandfire.data.managers.PrefManager;
import com.github.polurival.iceandfire.mvp.presenters.SplashPresenter;

/**
 * Created by Polurival
 * on 24.10.2016.
 */

public class SplashModel {

    private DataManager mDataManager = DataManager.getInstance();

    public SplashModel() {
    }

    public boolean isDBPrepared() {
        return PrefManager.isDBPrepared();
    }

    public void saveDBPreparedFlag() {
        PrefManager.saveDBPrepared(true);
    }

    public void loadHouse(int housePos) {
        mDataManager.loadHouse(housePos);
    }

    public void setOnDataPreparedListener(SplashPresenter onDataPreparedListener) {
        mDataManager.setOnDataPreparedListener(onDataPreparedListener);
    }
}
