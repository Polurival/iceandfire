package com.github.polurival.iceandfire.data.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.github.polurival.iceandfire.utils.ConstantManager.Column;
import static com.github.polurival.iceandfire.utils.ConstantManager.DB_NAME;
import static com.github.polurival.iceandfire.utils.ConstantManager.DB_VERSION;
import static com.github.polurival.iceandfire.utils.ConstantManager.Table;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper INSTANCE = null;

    private DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static DBHelper getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DBHelper(context);
        }
        return INSTANCE;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateDatabase(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateDatabase(db, oldVersion, newVersion);
    }

    private void updateDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {
            createHouseTable(db);
            createCharacterTable(db);
        }
    }

    private void createHouseTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Table.HOUSE + " (" +
                Column.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Column.URL + " TEXT, " +
                Column.WORDS + " TEXT);"
        );
    }

    private void createCharacterTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Table.CHARACTER + " (" +
                Column.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Column.URL + " TEXT, " +
                Column.NAME + " TEXT, " +
                Column.BORN + " TEXT, " +
                Column.DIED + " TEXT, " +
                Column.TITLES + " TEXT, " +
                Column.AlIASES + " TEXT, " +
                Column.FATHER + " TEXT, " +
                Column.MOTHER + " TEXT, " +
                Column.ALLEGIANCES + " TEXT);"
        );
    }
}
