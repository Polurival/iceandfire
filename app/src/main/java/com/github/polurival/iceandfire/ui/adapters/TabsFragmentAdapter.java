package com.github.polurival.iceandfire.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.github.polurival.iceandfire.ui.fragments.TabFragment;

import java.util.HashMap;
import java.util.Map;

import static com.github.polurival.iceandfire.utils.ConstantManager.LANNISTER_ID;
import static com.github.polurival.iceandfire.utils.ConstantManager.LOG_PREFIX;
import static com.github.polurival.iceandfire.utils.ConstantManager.STARK_ID;
import static com.github.polurival.iceandfire.utils.ConstantManager.TARGARYEN_ID;


/**
 * Created by Polurival
 * on 15.10.2016.
 */

public class TabsFragmentAdapter extends FragmentPagerAdapter {

    private static final String TAG = LOG_PREFIX + TabsFragmentAdapter.class.getSimpleName();

    private Map<Integer, TabFragment> mTabs;
    private Context mContext;

    public TabsFragmentAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;

        initTabsMap();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabs.get(position).getTitle();
    }

    @Override
    public Fragment getItem(int position) {
        return mTabs.get(position);
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

    @SuppressLint("UseSparseArrays")
    private void initTabsMap() {
        Log.d(TAG, "initTabsMap");

        mTabs = new HashMap<>();

        mTabs.put(0, TabFragment.newInstance(mContext, LANNISTER_ID));
        mTabs.put(1, TabFragment.newInstance(mContext, STARK_ID));
        mTabs.put(2, TabFragment.newInstance(mContext, TARGARYEN_ID));
    }
}
