package com.github.polurival.iceandfire.utils;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public interface ConstantManager {

    String LOG_PREFIX = "iceAndFireLog-";

    String CHARACTER_URL = "startCharacterActivity";

    int TAB_LANNISTER = 0;
    int TAB_STARK = 1;
    int TAB_TARGARYEN = 2;

    String HOUSE_ID = "houseId";
    String LANNISTER_ID = "229";
    String STARK_ID = "362";
    String TARGARYEN_ID = "378";
    String HOUSES[] = new String[]{LANNISTER_ID, STARK_ID, TARGARYEN_ID};

    String DB_NAME = "iceandfire.db";
    int DB_VERSION = 1;
    String IS_DB_PREPARED = "isDBPrepared";

    interface Table {
        String HOUSE = "house";
        String CHARACTER = "character";
    }

    interface Column {
        String ID = "_id";
        String URL = "url";
        String WORDS = "words";
        String NAME = "name";
        String BORN = "born";
        String DIED = "died";
        String TITLES = "titles";
        String AlIASES = "aliases";
        String FATHER = "father";
        String MOTHER = "mother";
        String ALLEGIANCES = "allegiances";
    }
}
