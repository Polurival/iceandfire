package com.github.polurival.iceandfire.utils;


import android.app.Application;
import android.content.Context;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public class IceAndFireApp extends Application {

    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;
    }

    public static Context getContext() {
        return sContext;
    }
}
