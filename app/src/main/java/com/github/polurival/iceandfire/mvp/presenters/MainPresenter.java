package com.github.polurival.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;

import com.github.polurival.iceandfire.mvp.views.IMainView;
import com.github.polurival.iceandfire.ui.adapters.TabsFragmentAdapter;

/**
 * Created by Polurival
 * on 24.10.2016.
 */

public class MainPresenter implements IMainPresenter {

    private static MainPresenter sMainPresenter = new MainPresenter();
    private IMainView mMainView;

    private MainPresenter() {
    }

    public static MainPresenter getInstance() {
        return sMainPresenter;
    }

    @Override
    public void takeView(IMainView mainView) {
        mMainView = mainView;
    }

    @Override
    public void dropView() {
        mMainView = null;
    }

    @Nullable
    @Override
    public IMainView getView() {
        return mMainView;
    }

    @Override
    public void setAdapterToViewPager() {
        if (getView() != null) {
            TabsFragmentAdapter mAdapter = new TabsFragmentAdapter(
                    getView().getContext(),
                    getView().getSupportFragmentManager()
            );
            getView().setAdapterToViewPager(mAdapter);
        }
    }

    @Override
    public void clickOnLannisterTab() {
        if (getView() != null) {
            getView().showLannisterTab();
        }
    }

    @Override
    public void clickOnStarkTab() {
        if (getView() != null) {
            getView().showStarkTab();
        }
    }

    @Override
    public void clickOnTargaryenTab() {
        if (getView() != null) {
            getView().showTargaryenTab();
        }
    }
}
