package com.github.polurival.iceandfire.mvp.views;

/**
 * Created by Polurival
 * on 23.10.2016.
 */

public interface ISplashView {

    void showProgress();

    void hideProgress();

    void startMainActivityWithDelay();

    void startMainActivity();
}
