package com.github.polurival.iceandfire.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.polurival.iceandfire.R;
import com.github.polurival.iceandfire.data.managers.DataManager;
import com.github.polurival.iceandfire.data.storage.models.CharacterListModel;

import java.util.List;

import static com.github.polurival.iceandfire.utils.ConstantManager.LANNISTER_ID;
import static com.github.polurival.iceandfire.utils.ConstantManager.LOG_PREFIX;
import static com.github.polurival.iceandfire.utils.ConstantManager.STARK_ID;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public class CharacterListAdapter extends RecyclerView.Adapter<CharacterListAdapter.ViewHolder> {

    private static final String TAG = LOG_PREFIX + CharacterListAdapter.class.getSimpleName();

    private Context mContext;
    private String mHouseId;
    private List<CharacterListModel> mCharactersList;

    private final ItemClickListener clickListener;

    public CharacterListAdapter(Context context, String houseId, ItemClickListener clickListener) {
        mContext = context;
        mHouseId = houseId;
        this.clickListener = clickListener;

        mCharactersList = DataManager.getInstance().getCharactersFromDB(houseId);
    }

    public interface ItemClickListener {
        void onClick(String url, String houseId);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private String mUrl;
        private ImageView mHouseIcon;
        private TextView mCharacterName;
        private TextView mCharacterTitles;

        ViewHolder(View itemView) {
            super(itemView);

            mHouseIcon = (ImageView) itemView.findViewById(R.id.character_item_house_icon);
            mCharacterName = (TextView) itemView.findViewById(R.id.character_item_name);
            mCharacterTitles = (TextView) itemView.findViewById(R.id.character_item_titles);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClick(mUrl, mHouseId);
                }
            });
        }

        private void bindHouseIcon() {
            int iconResId;
            if (LANNISTER_ID.equals(mHouseId)) {
                iconResId = R.drawable.lannister_icon;
            } else if (STARK_ID.equals(mHouseId)) {
                iconResId = R.drawable.stark_icon;
            } else {
                iconResId = R.drawable.targaryen_icon;
            }
            mHouseIcon.setImageResource(iconResId);
        }

        private void setUrl(String url) {
            mUrl = url;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = inflater.inflate(R.layout.fragment_character_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String url = mCharactersList.get(position).getUrl();
        holder.setUrl(url);

        holder.bindHouseIcon();

        String name = mCharactersList.get(position).getName();
        holder.mCharacterName.setText(name);

        String titles = mCharactersList.get(position).getTitles();
        holder.mCharacterTitles.setText(titles);
    }

    @Override
    public int getItemCount() {
        return mCharactersList.size();
    }
}
