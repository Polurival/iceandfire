package com.github.polurival.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;
import android.util.Log;

import com.github.polurival.iceandfire.R;
import com.github.polurival.iceandfire.data.storage.models.CharacterDetailModel;
import com.github.polurival.iceandfire.mvp.models.CharacterModel;
import com.github.polurival.iceandfire.mvp.views.ICharacterView;

import static com.github.polurival.iceandfire.utils.ConstantManager.LANNISTER_ID;
import static com.github.polurival.iceandfire.utils.ConstantManager.STARK_ID;

/**
 * Created by Polurival
 * on 24.10.2016.
 */

public class CharacterPresenter implements ICharacterPresenter {

    private static CharacterPresenter sCharacterPresenter = new CharacterPresenter();
    private CharacterModel mCharacterModel;
    private ICharacterView mCharacterView;

    private CharacterDetailModel mCharacter;
    private String mHouseId;

    private CharacterPresenter() {
        mCharacterModel = new CharacterModel();
    }

    public static CharacterPresenter getInstance() {
        return sCharacterPresenter;
    }

    @Override
    public void takeView(ICharacterView characterView) {
        mCharacterView = characterView;
    }

    @Override
    public void dropView() {
        mCharacterView = null;
    }

    @Nullable
    @Override
    public ICharacterView getView() {
        return mCharacterView;
    }

    @Override
    public void setHouseId(String houseId) {
        mHouseId = houseId;
    }

    @Override
    public void getCharacterFromDB(String url) {
        mCharacter = mCharacterModel.getCharacterFromDB(url, mHouseId);
    }

    @Override
    public void setTitle() {
        if (getView() != null) {
            getView().setTitle(mCharacter.getName());
        }
    }

    @Override
    public void initLogo() {
        if (getView() != null) {
            if (LANNISTER_ID.equals(mHouseId)) {
                getView().setLannisterLogo();
            } else if (STARK_ID.equals(mHouseId)) {
                getView().setStarkLogo();
            } else {
                getView().setTargaryenLogo();
            }
        }
    }

    @Override
    public void initTextViews() {
        if (getView() != null) {
            getView().setWords(mCharacter.getWords());
            getView().setBorn(mCharacter.getBorn());
            getView().setDied(mCharacter.getDied());
            getView().setTitles(mCharacter.getTitles());
            getView().setAliases(mCharacter.getAliases());
            getView().setFather(mCharacter.getFather());
            getView().setMother(mCharacter.getMother());
        }
    }
}
