package com.github.polurival.iceandfire.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.polurival.iceandfire.R;
import com.github.polurival.iceandfire.ui.activities.CharacterActivity;
import com.github.polurival.iceandfire.ui.adapters.CharacterListAdapter;
import com.github.polurival.iceandfire.ui.views.ItemDivider;

import static com.github.polurival.iceandfire.utils.ConstantManager.CHARACTER_URL;
import static com.github.polurival.iceandfire.utils.ConstantManager.HOUSE_ID;
import static com.github.polurival.iceandfire.utils.ConstantManager.LANNISTER_ID;
import static com.github.polurival.iceandfire.utils.ConstantManager.LOG_PREFIX;
import static com.github.polurival.iceandfire.utils.ConstantManager.STARK_ID;
import static com.github.polurival.iceandfire.utils.ConstantManager.TARGARYEN_ID;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public class TabFragment extends Fragment {

    private static final String TAG = LOG_PREFIX + TabFragment.class.getSimpleName();

    private Context mContext;
    private String mHouseId;
    private String mTitle;

    private CharacterListAdapter mAdapter;

    public void setContext(Context context) {
        mContext = context;
    }

    public void setTitle(String houseId) {
        if (LANNISTER_ID.equals(houseId)) {
            mTitle = mContext.getResources().getString(R.string.menu_item_lannisters);
        } else if (STARK_ID.equals(houseId)) {
            mTitle = mContext.getResources().getString(R.string.menu_item_starks);
        } else if (TARGARYEN_ID.equals(houseId)) {
            mTitle = mContext.getResources().getString(R.string.menu_item_targaryen);
        }
    }

    public String getTitle() {
        return mTitle;
    }


    public static TabFragment newInstance(Context context, String houseId) {
        Log.d(TAG, "newInstance " + houseId);

        Bundle args = new Bundle();
        args.putString(HOUSE_ID, houseId);

        TabFragment fragment = new TabFragment();
        fragment.setContext(context);
        fragment.setArguments(args);
        fragment.setTitle(houseId);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mHouseId = getArguments().getString(HOUSE_ID);
        Log.d(TAG, "onCreate " + mHouseId);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");

        View v = inflater.inflate(R.layout.fragment_tab, container, false);

        if (mAdapter == null) {
            mAdapter = new CharacterListAdapter(mContext, mHouseId,
                    new CharacterListAdapter.ItemClickListener() {
                        @Override
                        public void onClick(String url, String houseId) {
                            Intent intent = new Intent(mContext, CharacterActivity.class);
                            intent.putExtra(CHARACTER_URL, url);
                            intent.putExtra(HOUSE_ID, houseId);
                            startActivity(intent);
                        }
                    });
        }

        RecyclerView rv = (RecyclerView) v.findViewById(R.id.recycler_view);
        rv.setLayoutManager(new LinearLayoutManager(mContext));
        rv.setAdapter(mAdapter);
        rv.addItemDecoration(new ItemDivider(getContext()));
        rv.setHasFixedSize(true);

        return v;
    }
}
