package com.github.polurival.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;

import com.github.polurival.iceandfire.mvp.views.ICharacterView;

/**
 * Created by Polurival
 * on 24.10.2016.
 */

public interface ICharacterPresenter {

    void takeView(ICharacterView mainView);

    void dropView();

    @Nullable
    ICharacterView getView();

    void setHouseId(String houseId);

    void getCharacterFromDB(String url);

    void setTitle();

    void initLogo();

    void initTextViews();
}
