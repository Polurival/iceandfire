package com.github.polurival.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;

import com.github.polurival.iceandfire.mvp.views.ISplashView;

/**
 * Created by Polurival
 * on 23.10.2016.
 */

public interface ISplashPresenter {

    void takeView(ISplashView splashView);

    void dropView();

    @Nullable
    ISplashView getView();

    void startMainActivityWithDelay();

    void startMainActivity();

    boolean isDBPrepared();

    void loadHouses();
}
