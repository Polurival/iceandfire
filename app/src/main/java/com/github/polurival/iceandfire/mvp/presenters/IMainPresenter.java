package com.github.polurival.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;

import com.github.polurival.iceandfire.mvp.views.IMainView;

/**
 * Created by Polurival
 * on 24.10.2016.
 */

public interface IMainPresenter {

    void takeView(IMainView mainView);

    void dropView();

    @Nullable
    IMainView getView();

    void setAdapterToViewPager();

    void clickOnLannisterTab();

    void clickOnStarkTab();

    void clickOnTargaryenTab();
}
