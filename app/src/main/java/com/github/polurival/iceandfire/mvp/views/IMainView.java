package com.github.polurival.iceandfire.mvp.views;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.github.polurival.iceandfire.ui.adapters.TabsFragmentAdapter;

/**
 * Created by Polurival
 * on 24.10.2016.
 */

public interface IMainView {

    Context getContext();

    FragmentManager getSupportFragmentManager();

    void setAdapterToViewPager(TabsFragmentAdapter adapter);

    void showLannisterTab();

    void showStarkTab();

    void showTargaryenTab();
}
