package com.github.polurival.iceandfire.data.managers;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.github.polurival.iceandfire.utils.IceAndFireApp;

import static com.github.polurival.iceandfire.utils.ConstantManager.IS_DB_PREPARED;

/**
 * Created by Polurival
 * on 15.10.2016.
 */

public class PrefManager {

    private static SharedPreferences sPreferences
            = PreferenceManager.getDefaultSharedPreferences(IceAndFireApp.getContext());

    public static void saveDBPrepared(boolean isDBPrepared) {
        SharedPreferences.Editor editor = sPreferences.edit();
        editor.putBoolean(IS_DB_PREPARED, isDBPrepared);
        editor.apply();
    }

    public static boolean isDBPrepared() {
        return sPreferences.getBoolean(IS_DB_PREPARED, false);
    }
}
